#===============================================================================
#' @author:      Victor Faner
#' @date:        2019-10-24
#' @description: BUAN 6357.501 - HW5
#' @details:     Demonstration of Bootstrapping techniques using OLS
#===============================================================================
submit <- T

if (submit) {
  setwd("c:/data/BUAN6357/HW_5");
  source("prep.txt", echo = T)
} else setwd("/home/victor/Documents/BUAN6357/HW5")

#===============================================================================
# Begin script Part A
#===============================================================================

# Library imports, parameterizations ===========================================
library(tidyverse)
library(data.table)
library(broom)

data(airquality)

seed <- 436604030
raw <- airquality %>%
  select(-Day) %>%
  mutate(Month = as.factor(Month)) %>%
  .[complete.cases(.), ] %>%
  as.data.table()

n <- nrow(raw)
loc <- seq(n)
b <- 500
grp <- rep(1:b, each = n)  # Array of bootstrap groups for use in group by

# Classic bootstrap parameters
set.seed(seed)
cl.idx <- replicate(b, sample(n, replace = T), simplify = T) %>%
  as.vector()

# Balanced Bootstrap parameters
set.seed(seed)
bal.idx <- sample(rep(loc, b))  # Define sample indices

# Classic Bootstrap: Loop ======================================================
cl.models <- data.frame()

for (i in 1:b) {
  curr.idx <- cl.idx[grp == i]
  model <- lm(Ozone ~ Solar.R + Wind + Temp + Month, 
              data = raw[curr.idx, ]) %>% tidy()
  cl.models <- rbind(cl.models, model)
}

cl.loop <- cl.models %>%
  as.data.table()

# Classic Bootstrap: data.table ================================================
cl.samples <- data.table(cl.idx, grp)  # Define goruping data table
cl.dt <- cl.samples[,
                    tidy(lm(Ozone ~ Solar.R + Wind + Temp + Month, 
                            data = raw[cl.idx, ])),
                    by = grp]

# Balanced Bootstrap: Loop =====================================================
bal.models <- data.frame()

for (i in 1:b) {
  curr.idx <- bal.idx[grp == i]
  model <- lm(Ozone ~ Solar.R + Wind + Temp + Month, 
              data = raw[curr.idx, ]) %>% tidy()
  bal.models <- rbind(bal.models, model)
}

bal.loop <- bal.models %>%
  as.data.table()

# Balanced Bootstrap: data.table ===============================================
bal.samples <- data.table(bal.idx, grp)  # Define grouping data table
bal.dt <- bal.samples[,
                      tidy(lm(Ozone ~ Solar.R + Wind + Temp + Month, 
                            data = raw[bal.idx, ])),
                      by = grp]

#===============================================================================
# End script Part A
#===============================================================================
if (submit) source("validate.txt", echo = T) else {  
  # debugging
  print(table(cl.idx))
  print(table(bal.idx))
  print(all.equal(cl.loop, cl.dt[ , -1, with = F]))
  print(all.equal(bal.loop, bal.dt[ , -1, with = F]))
}
